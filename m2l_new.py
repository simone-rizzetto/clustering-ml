from __future__ import division

import optparse
import sys
import operator
import math
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from sklearn import neighbors
import numpy as np
from numpy import array
from sklearn import cross_validation
from textwrap import dedent
from sklearn import linear_model
from sklearn.cross_validation import KFold
from sklearn.neural_network import BernoulliRBM
from sklearn.naive_bayes import GaussianNB
import random
from itertools import groupby
from sklearn.feature_selection import VarianceThreshold
from sklearn import preprocessing
from sklearn import svm
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import chi2


def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False


def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)

def read_unip(fname):
    #t_exp={}
    mapping={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        genes=line.split('\t')
        x=0
        #for g in genes:
        #    if x>0:
        #       mapping[g]=genes[0]
        #    x+=1
        #print genes[1],
        #print genes[0]
        mapping[genes[0]]=genes[1]
    
    return mapping

def read_unip(fname):
    #t_exp={}
    mapping={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        genes=line.split('\t')
        x=0
        #for g in genes:
        #    if x>0:
        #       mapping[g]=genes[0]
        #    x+=1
        #print genes[1],
        #print genes[0]
        mapping[genes[0]]=genes[1]
    
    return mapping

def read_compl(fname, mapping, compl2):
    #t_exp={}
    compl={}
    x=1
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        prots=line.split('\t')
        row = []
        for p in prots:
            if p in mapping:
               compl2[mapping[p]]=x
               row.append(mapping[p])
        compl[x]=row
        #    if x>0:
        #       mapping[g]=genes[0]
        
        x+=1
        #print genes[1],
        #print genes[0]
    
    return compl   

def read_compl_c1(fname, mapping, compl2):
    #t_exp={}
    compl={}
    x=1
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        prots=line.split(' ')
        row = []
        for p in prots:
            compl2[p]=x
            row.append(p)
        compl[x]=row
        #    if x>0:
        #       mapping[g]=genes[0]
        
        x+=1
        #print genes[1],
        #print genes[0]
    
    return compl

def read_int(fname, mapping):
    #t_exp={}
    inter={}
    x=1
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        prots=line.split('\t')
        if prots[0] in inter:
           inter[prots[0]]+=1
        else:
             inter[prots[0]]=1
        if prots[1] in inter:
           inter[prots[1]]+=1
        else:
             inter[prots[1]]=1
        #print genes[1],
        #print genes[0]
    
    return inter

def read_texp(fname):
    #t_exp={}
    mapping={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        genes=line.split('\t')
        x=0
        #for g in genes:
        #    if x>0:
        #       mapping[g]=genes[0]
        #    x+=1
        #print genes[1],
        #print genes[0]
        mapping[genes[0]]=genes[1]
    
    return mapping
    
    
def read_ccel(fname):
    #t_exp={}
    ccel={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        line=line.replace('"', '')
        words=line.split(',')

        row=[words[3],words[4],words[5],words[6]] #gender site histology histology-sub
        ccel[words[0]]=row
        ccel[words[1]]=row

    return ccel

def count_mutations(fname):
    #t_exp={}
    count={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        line=line.replace('"', '')
        words=line.split('\t')
        ccel=words[2].replace('-', '')

        if ccel in count:
           if words[0] in count[ccel]:
              count[ccel][words[0]]+=1
              #print ccel+"-c-"+words[0]
           else:
                count[ccel][words[0]]=1
                #print ccel+"-b-"+words[0]
        else:
             count[ccel] = {}
             count[ccel][words[0]]=1
             #print ccel+"-a-"+words[0]



    return count
    
def count_smutations(fname):
    #t_exp={}
    count={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        line=line.replace('"', '')
        words=line.split('\t')
        ccel=words[0].replace('-', '')

        if ccel in count:
           count[ccel]+=1
              #print ccel+"-c-"+words[0]
        else:
             count[ccel]=1
                #print ccel+"-b-"+words[0]
             #print ccel+"-a-"+words[0]
        #print ccel+" "+str(count[ccel])


    return count
    
def cancerGenes(fname):
    #t_exp={}
    genes=[]
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        words=line.split('\t')
        genes.append(words[1])


    return genes

def read_texp(fname, fname2):
    t_exp={}
    mapping={}
    for line in open(fname2):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        genes=line.split('\t')
        x=0
        for g in genes:
            if x>0:
               mapping[g]=genes[0]
            x+=1
        #print genes[1],
        #print genes[0]
        #mapping[genes[1]]=genes[0]


    nline=0
    for line in open(fname):
        if nline>0:
          line=line.replace("\n", "")
          line=line.replace("\r", "")
          genes=line.split('\t')
          tissues = []
          x=0
          for t in genes:
              if x>0:
                 tissues.append(t)
              x+=1
          #print mapping[genes[0]]
          if genes[0] in mapping:
             t_exp[mapping[genes[0]]]=tissues
        nline+=1
    #print t_exp
    return t_exp

def read_matrix(fname, fname2, fname3, fname_cnv, ge,es,ge_t, n_gene, n_ccel, ccel_f, t_exp, compl2, ccel_ann, mutations, smutations, cancer_genes,ge2,ge_3, essent,geneInEssent,fname_cnv2, priority, compl, interaction, compl_c1, ppi):
    print len(compl)
    print len(compl2)
    nline=0
    id_type = []
    id_site = []
    id_site2 = []
    id_hist = []
    id_hists = []

    ccel_type = {}
    ccel_site = {}
    ccel_site2 = {}
    ccel_gender = {}
    ccel_hist = {}
    ccel_hists = {}
    for line in open(ccel_f):
        line=line.replace("\n", "")
        line=line.replace("\r", "")

        ccel=line.split('\t')
        if not nline==0:
           ccel_type[ccel[0]]=ccel[2]
           ccel_site[ccel[0]]=ccel[3]
           found=False
           if ccel[0] in ccel_ann:
              found=True
              ccel_gender[ccel[0]]=ccel_ann[ccel[0]][0]
              ccel_site2[ccel[0]]=ccel_ann[ccel[0]][1]
              ccel_hist[ccel[0]]=ccel_ann[ccel[0]][2]
              ccel_hists[ccel[0]]=ccel_ann[ccel[0]][3]
           if ccel[1] in ccel_ann and not found:
              found=True
              ccel_gender[ccel[0]]=ccel_ann[ccel[1]][0]
              ccel_site2[ccel[0]]=ccel_ann[ccel[1]][1]
              ccel_hist[ccel[0]]=ccel_ann[ccel[1]][2]
              ccel_hists[ccel[0]]=ccel_ann[ccel[1]][3]
           if not found:
              print "MISSING!"+ccel[0]
              ccel_gender[ccel[0]]=''
              ccel_site2[ccel[0]]=''
              ccel_hist[ccel[0]]=''
              ccel_hists[ccel[0]]=''

        nline+=1
    for s in ccel_site:
        if not (ccel_site[s] in id_site):
           id_site.append(ccel_site[s])

    for s in ccel_type:
        if not (ccel_type[s] in id_type):
           id_type.append(ccel_type[s])

    for s in ccel_site2:
        if not (ccel_site2[s] in id_site2):
           id_site2.append(ccel_site2[s])

    for s in ccel_hist:
        if not (ccel_hist[s] in id_hist):
           id_hist.append(ccel_hist[s])

    for s in ccel_hists:
        if not (ccel_hists[s] in id_hists):
           id_hists.append(ccel_hists[s])

    headers = []
    nline=0
    expr = {}
    expr1 = {}
    out_file = open("training_x.txt","w")
    out_file4 = open("training_new.txt","w")
    out_file2 = open("training_y.txt","w")
    out_file3 = open("prediction.gct","w")
    out_file_f = open("features.txt","w")
    exp_avg = 0
    genes=[]
    cnv_g=[]
    p2g={}
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data
                    p2g[ccel[0]]=ccel[1]
                    expr[str(headers[x])+str(ccel[1])]=float(data)
                    expr1[str(headers[x])+str(ccel[0])]=float(data)
                    if not ccel[0] in genes:
                       genes.append(ccel[0])
                    exp_avg+=float(data)
                 x+=1
        nline+=1
    exp_avg=exp_avg/len(expr)
    print exp_avg
    exp_std=0
    for e in expr:
        exp_std+=(float(expr[e])-exp_avg)*(float(expr[e])-exp_avg)
    exp_std=math.sqrt(exp_std/len(expr))
    print exp_std
    headers = []
    nline=0
    cnv = {}
    for line in open(fname_cnv):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data
                    cnv[str(headers[x])+str(ccel[1])]=float(data)
                    if not ccel[0] in cnv_g:
                       cnv_g.append(ccel[0])
                 x+=1
        nline+=1

    headers_t = []
    nline=0
    cnv2 = {}
    for line in open(fname_cnv2):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers_t.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data
                    cnv2[str(headers_t[x])+str(ccel[1])]=float(data)
                 x+=1
        nline+=1
    headers = []
    nline=0
    tmap={}
    g_count=0
    intr=0
    tot_sp=0
    essent2={}
    essent3={}
    all_info=[]
    out_file.write("Gene\tCCEL\tGene Expression\tCNV\tType1\tSite1\tGender\tSite2\tHistology\tHistology subtype\t# SNPs\t# structural mutations\tExpressed in site\tKnown cancer gene\tEssentiality\n")
    for line in open(fname2):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers.append(h)
               essent3[h]=[]
        else:
             x=0
             row3=[ccel[1]]
             es_full=[]
             essent2[ccel[1]]=[]
             for data in ccel:
                 if x>=2 and str(headers[x])+str(ccel[1]) in expr and  str(headers[x])+str(ccel[1]) in cnv:
                    all_info.append(ccel[1])
                    expr_in_site=0
                    #print ccel[1]
                    tmap[g_count]=ccel[1]
                    if not ccel[1] in geneInEssent:
                       geneInEssent.append(ccel[1])
                    g_count+=1
                    '''
                    if ccel[1] in t_exp:
                      if ccel_site[str(headers[x])] in t_exp[ccel[1]]:
                         expr_in_site=1
                      #print ccel[1]+" ",
                      #print ccel_site[str(headers[x])]+" ",
                      #for t in t_exp[ccel[1]]:
                          #print t+" ",
                      #print ""
                    isCancerGene=0
                    if ccel[1] in cancer_genes:
                       isCancerGene=1
                    #c=0
                    #if ccel[1] in compl2:
                    #   c=compl[ccel[1]]
                    n_mutations=0
                    if (str(headers[x]) in mutations):
                       if ccel[1] in mutations[str(headers[x])]:
                          n_mutations=mutations[str(headers[x])][ccel[1]]

                    if not (str(headers[x]) in smutations):
                       smutations[str(headers[x])]=0
                    out_file.write(ccel[1]+"\t"+headers[x]+"\t"+str(expr[str(headers[x])+str(ccel[1])])+"\t"+str(cnv[str(headers[x])+str(ccel[1])])+"\t"+str(ccel_type[str(headers[x])])+"\t"+str(ccel_site[str(headers[x])])+"\t"+str(ccel_gender[str(headers[x])])+"\t"+str(ccel_site2[str(headers[x])])+"\t"+str(ccel_hist[str(headers[x])])+"\t"+str(ccel_hists[str(headers[x])])+"\t"+str(n_mutations)+"\t"+str(smutations[str(headers[x])])+"\t"+str(expr_in_site)+"\t"+str(isCancerGene)+"\t"+data+"\n")
                    out_file2.write(data+"\t")
                    #ge2.append([nline,x,float(expr[str(headers[x])+str(ccel[1])])])
                    #ge2.append(full)
                    row3.append(float(expr[str(headers[x])+str(ccel[1])]))
                    #ge2.append([float(expr[str(headers[x])+str(ccel[1])])])
                    row=[float(expr[str(headers[x])+str(ccel[1])]),float(cnv[str(headers[x])+str(ccel[1])])]
                    tissID=0
                    for id1,t in enumerate(id_type):
                        if t==str(ccel_type[str(headers[x])]):
                           #row.append(1)
                           tissID=id1
                        #else:
                             #row.append(0)
                    row.append(tissID)
                    siteID=0
                    for id1,s in enumerate(id_site):
                        if s==str(ccel_site[str(headers[x])]):
                            siteID=id1
                    #row.append(siteID)
                    gender=0
                    if ccel_gender[str(headers[x])]=='M':
                       gender=1
                    if ccel_gender[str(headers[x])]=='F':
                       gender=2
                    #row.append(gender)
                    siteID=0
                    for id1,s in enumerate(id_site2):
                        if s==str(ccel_site2[str(headers[x])]):
                            siteID=id1
                    row.append(siteID)


                    siteID=0
                    for id1,s in enumerate(id_hist):
                        if s==str(ccel_hist[str(headers[x])]):
                            siteID=id1
                    row.append(siteID)

                    siteID=0


                    for id1,s in enumerate(id_hists):
                        if s==str(ccel_hists[str(headers[x])]):
                            siteID=id1

                    row.append(siteID)

                    row.append(n_mutations)
                    row.append(smutations[str(headers[x])])
                    #row.append(expr_in_site)
                    row.append(isCancerGene)
                    #for c in compl2:
                    #    if ccel[1] in compl2:
                    #       row.append(1)
                    #    else:
                    #        row.append(0)
                    ge.append(row)
                    #ge.append([float(expr[str(headers[x])+str(ccel[1])]),float(cnv[str(headers[x])+str(ccel[1])]),str(ccel_type[str(headers[x])]),str(ccel_site[str(headers[x])])])
                    '''
                    es.append(float(data))
                    es_full.append(float(data))
                    essent2[ccel[1]].append(float(data))
                    essent3[headers[x]].append(float(data))
                    #print ccel[1]+"\t"+headers[x]+"\t"+expr[str(headers[x])+str(ccel[1])]+"\t"+data
                 elif x>=2:
                    if not ccel[1] in geneInEssent:
                       geneInEssent.append(ccel[1])
                    essent2[ccel[1]].append(float(data))
                    essent3[headers[x]].append(float(data))

                 x+=1
             out_file2.write("\n")
             ge_3.append(row3)
             full=[]
             for c in headers:
                 if str(c)+ccel[1] in expr:
                    full.append([expr[str(c)+ccel[1]]])

             if len(es_full)>0 and len(full)>0:
                xt=np.array(full)
                yt=np.array(es_full)
                #knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)

                #kf = KFold(len(es_full), n_folds=10)
                sp=0
                #for traincv, testcv in kf:
                    #p=knn2.fit(xt[traincv],yt[traincv]).predict(xt[testcv])
                    #sp+=spearmanr(p,yt[testcv])[0]
                if not math.isnan(sp/10):
                   tot_sp=float(tot_sp)+float(sp/10)
                   intr+=1
                #print sp/10
                #print tot_sp/intr


        nline+=1
        
        

    nline=0
    #print len(row)
    expr2={}
    headers_t=[]
    for line in open(fname3):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers_t.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data

                    expr2[str(headers_t[x])+str(ccel[0])]=float(data)
                    ge_t.append([float(data)])
                 x+=1
             ccel=x
        nline+=1
    n_gene=nline
    



    c_features={}
    for c in headers:
        if c!="Name" and c!="Description":
           c_features[c]=[]
           #row.append(smutations[c])
           tissID=0
           for id1,t in enumerate(id_type):
               if t==str(ccel_type[c]):
                  c_features[c].append(1)
                           #tissID=id1
               else:
                    c_features[c].append(0)

               #row.append(tissID)
               #     siteID=0
               for id1,s in enumerate(id_site):
                   if s==str(ccel_site[c]):
                      c_features[c].append(1)
                            #siteID=id1
                   else:
                        c_features[c].append(0)
                    #row.append(siteID)
               gender=0
               if ccel_gender[c]=='M':
                  gender=1
               if ccel_gender[c]=='F':
                  gender=2
               c_features[c].append(gender)
                    #siteID=0
                    

               for id1,s in enumerate(id_site2):
                   if s==str(ccel_site2[c]):
                      c_features[c].append(1)
                   else:
                        c_features[c].append(0)
                            #siteID=id1
                    #row.append(siteID)
               #siteID=0
               for id1,s in enumerate(id_hist):
                   if s==str(ccel_hist[c]):
                      c_features[c].append(1)
                   else:
                        c_features[c].append(0)
                            #siteID=id1
                    #row.append(siteID)
               #siteID=0
               for id1,s in enumerate(id_hists):
                   if s==str(ccel_hists[c]):
                      c_features[c].append(1)
                   else:
                        c_features[c].append(0)
                            #siteID=id1

                    #row.append(siteID)
    full2=[]
    for c in headers_t:
        if c!="Name" and c!="Description":
           row2=[]
           for ex in genes:
               row2.append(expr2[c+ex])
           for ex in cnv_g:
               row2.append(float(cnv2[c+ex]))
           full2.append(row2)
    test=np.array(full2)

    full=[]
    full_y=[]
    for c in headers:
        if c!="Name" and c!="Description":
           row=[]
           for ex in genes:
               #if p2g[ex] in cancer_genes:
               row.append(expr1[c+ex])
           for ex in cnv_g:
               row.append(float(cnv[c+ex]))
           full.append(row)
           full_y.append(essent3[c])	
    xt=np.array(full)
    yt2=np.array(full_y)
    kf = KFold(len(full), n_folds=10)
    sp=0

    knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
    p=knn2.fit(xt,yt2).predict(test)
    '''
    print "#1.2"
    print str(len(priority))+"\t"+str(len(headers_t)-2)
    for c in headers_t:
        if c!="Name":
           print "\t",
        print c,
    print ""
    for i,g in enumerate(geneInEssent):
	print str(g)+"\t"+str(g),
	for c in p:
           print "\t"+str(c[i]),
	print ""
    '''	
    '''
    for traincv, testcv in kf:
	p=knn2.fit(xt[traincv],yt2[traincv]).predict(xt[testcv])
	#print len(p)
	#print p
	sp2=0
	for ig,g in enumerate(p[0]):
           pred[ig]=[]
		for c in 
	for i,r in enumerate(p):
           sp2+=spearmanr(r,yt2[testcv][i])[0]
	sp+=sp2/len(r)
    print sp/10
    '''
    #X_scaled = preprocessing.scale(xt)
    var=0.5
    
    while len(xt[0])>1000:
          var+=0.2
          sel = VarianceThreshold(threshold=(var))
          #xt=sel.fit_transform(xt)
          vs=sel.fit(xt)
          xt=vs.transform(xt)
          test=vs.transform(test)

    '''   
    new_xt=xt.tolist()	
    new_full=[]
    new_yt=[]
    for ig,g in enumerate(geneInEssent):
	tmp=new_xt
	for c,r in enumerate(tmp):
            #print r
            r.append(ig)
            r.append(c)
            
            #r=np.concatenate(r,[ig,c])
            new_full.append(r)
	#new_yt=new_yt+essent2[g]
    #xt=np.array(new_full)
    #yt=np.array(full_y)
    scalar = preprocessing.StandardScaler().fit(xt)
    xts = scalar.transform(xt)
    scalar = preprocessing.StandardScaler().fit(test)
    tts = scalar.transform(test)
    clf = linear_model.SGDRegressor()
	
    #clf.partial_fit(xts2,yt)
    for ig,g in enumerate(geneInEssent):
	#partial=[]
	#print g
	xts2=xts.tolist()
        yt=np.array(essent2[g])
	for c,r in enumerate(xts2):
            #print r
            r.append(ig/len(geneInEssent))
            #r=[ig,c]
            #r=[(ig/len(geneInEssent)),(c/len(xts2))]
            r.append(c/len(xts2))
            print yt[c],	
            for f in r:
		print " "+str(f),
            print ""
            #r=np.concatenate(r,[ig,c])
	
	xts2=np.array(xts2)
	#xt=np.array(X_scaled)
	clf.partial_fit(xts2,yt)
    print len(xts2)
    print len(xts2[0])
    print len(tts)
    print len(tts[0])
    print "#1.2"
    print str(len(geneInEssent))+"\t"+str(len(headers_t)-2)
    for c in headers_t:
        if c!="Name":
           print "\t",
        print c,
    print ""
    for ig,g in enumerate(geneInEssent):
	test2=tts.tolist()
	for c,r in enumerate(test2):
            #print r
            r.append(ig/len(geneInEssent))
            r.append(c/len(tts))
            
            #r=np.concatenate(r,[ig,c])
	test2=np.array(test2)
	#print test2
	#print len(test2)
	#print len(test2[0])
	p=clf.predict(test2)
	print str(g)+"\t"+str(g),
	for pr in p:
            print "\t"+str(pr),
	print ""
	
    #p=clf.predict(test2)	
    #print p	
    #print "avg_sp"
    #print tot_sp
    #test=[]

    #print "avg_sp:"+str(tot_sp/intr)
	
    error+=1
    '''
    '''
    file_pos = open("positive.txt","w")
    file_neg = open("negative.txt","w")
    #sub-challenge 3
    intr=0
    tot_sp=0
    selected_features=[]
    for i in range (0,100):
        #r=random.uniform(0,genes)
        r=random.randint(0,len(genes)-1)
        while r in selected_features:
              r=random.randint(0,len(genes)-1)
        selected_features.append(r)
        out_file_f.write("\t"+genes[r])

    out_file_f.write("\n")
    out_file_f.close()
    print "#1.2"
    print str(len(priority))+"\t"+str(len(headers_t)-2)
    for c in headers_t:
        if c!="Name":
           print "\t",
        print c,
    print ""
    full=[]
    full2=[]
    for c in headers:
        if c!="Name" and c!="Description":
           row=[]
           for ex in genes:
               #if p2g[ex] in cancer_genes:
               row.append(expr1[c+ex])
           for ex in cnv_g:
               row.append(float(cnv[c+ex]))
           row_n=[]
           for f in selected_features:
               row_n.append(row[f])
           full.append(row)
    xt=np.array(full)
    var=1.0

    while len(xt[0])>50:
          var+=0.2
          sel = VarianceThreshold(threshold=(var))
          xt=sel.fit_transform(xt)
    pxt=xt.tolist()
    full=[]
    for c in headers:
        if c!="Name" and c!="Description":
           row=[]
           for ex in cnv_g:
               row.append(float(cnv[c+ex]))
           row_n=[]
           for f in selected_features:
               row_n.append(row[f])
           full.append(row)
    xt2=np.array(full)
    var=0.0
    while len(xt2[0])>50:
          var+=0.2
          sel = VarianceThreshold(threshold=(var))
          xt2=sel.fit_transform(xt2)
    pxt2=xt2.tolist()
    print len(xt)
    print len(xt2)
    print len(xt[0])
    print len(xt2[0])
    for i,r in enumerate(pxt):
        r=r+pxt2[i]
    xt=np.array(pxt)
    print len(xt[0])

    xt=np.array(full)
    for c in headers_t:
        if c!="Name" and c!="Description":
           row2=[]
           for ex in genes:
               #if p2g[ex] in cancer_genes:
               row2.append(expr2[c+ex])
           for ex in cnv_g:
               row2.append(float(cnv2[c+ex]))
           row_n2=[]
           for f in selected_features:
               row_n2.append(row2[f])
           full2.append(row2)
    print len(xt[0])
    test=np.array(full2)
    for g in priority:

        yt=np.array(essent2[g])
        knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
        kf = KFold(len(essent2[g]), n_folds=10)
        sp=0
        for traincv, testcv in kf:
            p=knn2.fit(xt[traincv],yt[traincv]).predict(xt[testcv])
            sp+=spearmanr(p,yt[testcv])[0]
        #res=knn2.fit(xt,yt).predict(test)
        #print g+"\t"+g,
        #for p in res:
        #    print "\t"+str(p),
        #print ""

        if not math.isnan(sp/10):
           if (sp/10)<0.2:
              file_neg.write(g+"\n")
           else:
              file_pos.write(g+"\n")
           tot_sp=float(tot_sp)+float(sp/10)
           intr+=1
        #print sp/10
        #print tot_sp/intr
    file_neg.close()
    file_pos.close()
    print "avg_sp:"+str(tot_sp/intr)
    '''
    '''
    #sub-challenge 2

    intr=0
    tot_sp=0
    print "#1.2"
    print str(len(geneInEssent))+"\t"+str(len(headers_t)-2)
    for c in headers_t:
        if c!="Name":
           print "\t",
        print c,
    print ""
    for g in priority:
        full=[]
        full2=[]
        selected_features=[]
        out_file_f.write(g)
        cocompl=[g]
        #for cc in compl2:
        #    if g in compl2[cc]:
        #       cocompl=cocompl+compl2[cc]
        #for i in range (0,10):
        #    r=random.randint(0,len(genes)-1)
        #    while r in selected_features:
        #          r=random.randint(0,len(genes)-1)

        #    selected_features.append(r)
        #    out_file_f.write("\t"+genes[r])
        #out_file_f.write("\n")
        for c in headers:
            if c!="Name" and c!="Description":
               row=[]
               for ex in genes:
                   row.append(expr1[c+ex])  
               row_n=[]
               #for f in selected_features:
               #    row_n.append(row[f])
               for ex in genes:
                   if p2g[ex] in cocompl:
                      row_n.append(expr1[c+ex])
               full.append(row_n)
        for c in headers_t:
            if c!="Name" and c!="Description":
               row2=[]
               for ex in genes:
                   row2.append(expr2[c+ex])
               row_n2=[]
               for f in selected_features:
                   row_n2.append(row2[f])
               full2.append(row_n2)
        xt=np.array(full)
        test=np.array(full2)
        print len(xt[0])
        if len(xt[0])<=0:
           continue
        #print len(xt)
        #print len(essent2[g])
        yt=np.array(essent2[g])
        knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
        kf = KFold(len(essent2[g]), n_folds=10)
        sp=0
        for traincv, testcv in kf:
            p=knn2.fit(xt[traincv],yt[traincv]).predict(xt[testcv])
            sp+=spearmanr(p,yt[testcv])[0]

        if not math.isnan(sp/10):
           tot_sp=float(tot_sp)+float(sp/10)
           intr+=1
        print sp/10
        print tot_sp/intr

        #res=knn2.fit(xt,yt).predict(test)
        #print g+"\t"+g,
        #for p in res:
        #    print "\t"+str(p),
        #print ""

    print "avg_sp:"+str(tot_sp/intr)

    out_file_f.close()
    '''
    #sub-challenge 1


    #stage 1 ccel features
    intr=0
    tot_sp=0
    s1_prediction={}
    print "#1.2"
    print str(len(geneInEssent))+"\t"+str(len(headers_t)-2)
    for c in headers_t:
        if c!="Name":
           print "\t",
        print c,
    print ""

    full=[]
    full2=[]
    for c in headers:
        if c!="Name" and c!="Description":
           row=[]
           row=row+c_features[c]
           for ex in genes:
               row.append(expr1[c+ex])
           for ex in cnv_g:
               row.append(float(cnv[c+ex]))
           full.append(row)
           #print len(row)
    for c in headers_t:
        if c!="Name" and c!="Description":
           row2=[]
           for ex in genes:
               row2.append(expr2[c+ex])
           for ex in cnv_g:
               row2.append(float(cnv2[c+ex]))
           full2.append(row2)
    xt=np.array(full)
    test=np.array(full2)
    '''
    var=1.0

    while len(xt[0])>100:
          var+=0.2
          sel = VarianceThreshold(threshold=(var))
          vs=sel.fit(xt)
          xt=vs.transform(xt)
          test=vs.transform(test)
          #xt=sel.fit_transform(xt,)

    pxt=xt.tolist()
    '''
    '''
    i=0
    for c in headers:
        if c!="Name" and c!="Description":
           pxt[i]=pxt[i]+c_features[c]
           i+=1
    '''   
    #xt=np.array(pxt)
    #sel = VarianceThreshold(threshold=(1.8))
    #xt=sel.fit_transform(xt)
    print len(xt[0])
    inExp=0
    ninExp=0
    xt=np.array(full)
    #for g in geneInEssent:
    for g in priority:
        s1_prediction[g]={}
        #features=full
        features=[]
	'''
        for i,c in enumerate(headers):
            if (i-2)>=0:
               exp_list=[]
               cnv_list=[]
               for cc in compl2:
                   if g in compl2[cc]:
                      for p in compl2[cc]:
                          if ((str(c)+str(p)) in expr) and (not (p==g)):
                             exp_list.append(float(expr[str(c)+str(p)]))
                          if ((str(c)+str(p)) in cnv) and (not (p==g)):
                             cnv_list.append(float(cnv[str(c)+str(p)]))
               if (str(c)+str(g) in expr):
                  exp_list.append(expr[str(c)+str(g)])
               if (str(c)+str(g) in cnv):
                  cnv_list.append(cnv[str(c)+str(g)])
               e=np.array(exp_list)
               c=np.array(cnv_list)
               row=[]

               if len(exp_list)>0:
                  #print i-2
                  #print exp_list
                  #print np.mean(e)
                  inExp+=1
                  row=full[i-2]+[np.mean(e)]
                  row.append(np.std(e))
                  row.append(np.mean(c))
                  row.append(np.std(c))
                  row=row+exp_list
               else:
                    ninExp+=1
                    #print i-2
                    row=full[i-2]+[]
                    #row=[0]
               features.append(row)
	'''
        #print str(len(features[0]))+" - "+str(len(e))
        #xt=np.array(features)
        yt=np.array(essent2[g])
        xt=SelectKBest(chi2, k=500).fit_transform(xt, yt)
#        knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
        knn2 = svm.SVR()
	#rbm = BernoulliRBM(random_state=0, verbose=True)
	#logistic = linear_model.LogisticRegression()
	'''	
	knn2=Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])
	rbm.learning_rate = 0.06
	rbm.n_iter = 20
	rbm.n_components = 100
	logistic.C = 6000.0
	'''
        #print len(yt)
        #print len(test)
        #print len(essent2[g])
        #leaderboard prediction
        #res=knn2.fit(xt,yt).predict(test)
        #for ind,pr in enumerate(res):
        #    s1_prediction[g][ind]=pr
        #print g+"\t"+g,
        #for p in res:
        #    print "\t"+str(p),
        #print ""


        #10-fold crossvalidation on training dataset only
        kf = KFold(len(essent2[g]), n_folds=10)
        sp=0
        s1_prediction[g]={}
        for traincv, testcv in kf:
            #print len(xt[traincv])
            #print len(yt[traincv])
            rk=knn2.fit(xt[traincv],yt[traincv])
            #print len(xt[testcv])
            #for el in xt[testcv]:
            #    print len(el)
            p=rk.predict(xt[testcv])
            for ind,pr in enumerate(p):
                s1_prediction[g][testcv[ind]]=pr
            sp+=spearmanr(p,yt[testcv])[0]
        #print s1_prediction
        if not math.isnan(sp/10):
           tot_sp=float(tot_sp)+float(sp/10)
           intr+=1
        if intr%100==0:
           print inExp
           print ninExp
           print tot_sp/intr


    #print "avg_sp:"+str(tot_sp/intr)




    tot_sp=0
    intr=0
    for g in s1_prediction:
        row=[]
        yt=np.array(essent2[g])
        for c in s1_prediction[g]:
            row.append(s1_prediction[g][c])
        p1=np.array(row)
        if not (len(yt)==len(p1)):
           print g
           continue
        sp=spearmanr(p1,yt)[0]
        tot_sp=float(tot_sp)+float(sp)
        intr+=1
    print "stage1 avg sp:",
    print tot_sp/intr

    
    

    tot_sp=0
    intr=0
    #stage2 import gene features
    '''
    for c in s1_prediction[priority[0]]:
        train_x=[]
        train_y=[]
        for idg,g in enumerate(s1_prediction):
            isCancer=0
            if g in cancer_genes:
               isCancer=1
            n_ggi=0
            if g in interaction:
               n_ggi=interaction[g]
            n_ppi=0
            if g in ppi:
               n_ppi=ppi[g]
            #print s1_prediction[g][c]
            row=[s1_prediction[g][c],isCancer]
            #row=[idg]

            for cc in compl2:
                if g in compl2[cc]:
                   row.append(1)
                else:
                     row.append(0)

            train_x.append(row)
            #train_x.append([s1_prediction[g][c],isCancer])
            train_y.append(essent2[g][c])
        xt=np.array(train_x)
        yt=np.array(train_y)
        rk=knn2.fit(xt,yt)
        p=rk.predict(xt)
        for ind,pr in enumerate(p):
            s1_prediction[g][ind]=pr
        #print len(xt)
        #print len(xt[0])
        #print len(yt)

        kf = KFold(len(yt), n_folds=10)
        sp=0
        #s1_prediction[g]={}

        for traincv, testcv in kf:
            rk=knn2.fit(xt[traincv],yt[traincv])
            p=rk.predict(xt[testcv])
            for ind,pr in enumerate(p):
                s1_prediction[g][testcv[ind]]=pr
            if not math.isnan(spearmanr(p,yt[testcv])[0]):
               sp+=spearmanr(p,yt[testcv])[0]
               intr+=1
               tot_sp+=spearmanr(p,yt[testcv])[0]
        #print tot_sp
        print tot_sp/intr



    tot_sp=0
    intr=0
    avg_c={}
    s2_prediction=s1_prediction


    for cc in compl2:
        for c in s1_prediction[priority[0]]:
            avg_c[cc]=0
            prot=0
            for p in compl2[cc]:
                if p in s1_prediction:
                   avg_c[cc]+=s1_prediction[p][c]
                   prot+=1
            if prot > 0:
               avg_c[cc]=avg_c[cc]/prot
            for p in compl2[cc]:
                if p in s2_prediction:
                   s2_prediction[p][c]=avg_c[cc]

    for g in s2_prediction:
        row=[]
        yt=np.array(essent2[g])
        print str(g)+"\t"+str(g)
        for c in s2_prediction[g]:
            row.append(s2_prediction[g][c])
            print "\t"+str(s2_prediction[g][c]),
        print ""

    tot_sp=0
    intr=0
    avg_c={}
    s2_prediction=s1_prediction
    for cc in compl2:
        for c in s1_prediction[priority[0]]:
            avg_c[cc]=0
            prot=0
            for p in compl2[cc]:
                if p in s1_prediction:
                   avg_c[cc]+=s1_prediction[p][c]
                   prot+=1
            if prot > 0:
               avg_c[cc]=avg_c[cc]/prot
            for p in compl2[cc]:
                if p in s2_prediction:
                   s2_prediction[p][c]=avg_c[cc]

    for g in s2_prediction:
        row=[]
        yt=np.array(essent2[g])
        for c in s2_prediction[g]:
            row.append(s2_prediction[g][c])
        p1=np.array(row)
        if not (len(yt)==len(p1)):
           print g
           continue
        sp=spearmanr(p1,yt)[0]
        tot_sp=float(tot_sp)+float(sp)
        intr+=1

    print "stage2 sp:",
    print tot_sp/intr
    tot_sp=0
    intr=0
    for g in s1_prediction:
        row=[]
        yt=np.array(essent2[g])
        for c in s1_prediction[g]:
            row.append(s1_prediction[g][c])
        p1=np.array(row)
        if not (len(yt)==len(p1)):
           print g
           continue
        sp=spearmanr(p1,yt)[0]
        tot_sp=float(tot_sp)+float(sp)
        intr+=1
    print "stage2 avg sp:",
    print tot_sp/intr
    #print intr
    #stage 3 groups
    tot_sp=0
    intr=0

    s2_prediction={}
    for g in s1_prediction:
        full=[]
        ic=0
        for c in headers:
            if c!="Name" and c!="Description":
               row=[]
               for cc in compl2:
                   if g in compl2[cc]:
                      for p in compl2[cc]:
                          if p in s1_prediction:
                             row.append(s1_prediction[p][ic])
               full.append(row)
               ic+=1
        yt=np.array(essent2[g])
        xt=np.array(full)
        knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
        #print len(xt[0])
        s2_prediction[g]={}
        if len(xt[0])<=1:
           row=[]
           for c in s1_prediction[g]:
               row.append(s1_prediction[g][c])
               s2_prediction[g][c]=s1_prediction[g][c]
           p1=np.array(row)
           if not (len(yt)==len(p1)):
              print g
              continue
           sp=spearmanr(p1,yt)[0]
           tot_sp=float(tot_sp)+float(sp)
           intr+=1
           continue
        #leaderboard prediction
        #res=knn2.fit(xt,yt).predict(test)
        #print g+"\t"+g,
        #for p in res:
        #    print "\t"+str(p),
        #print ""


        #10-fold crossvalidation on training dataset only
        kf = KFold(len(essent2[g]), n_folds=10)
        sp=0
        lintr=0
        for traincv, testcv in kf:
            #print len(xt[traincv])
            #print len(yt[traincv])
            rk=knn2.fit(xt[traincv],yt[traincv])
            #print len(xt[testcv])
            #for el in xt[testcv]:
            #    print len(el)
            p=rk.predict(xt[testcv])
            for ind,pr in enumerate(p):
                s2_prediction[g][testcv[ind]]=pr
            if not math.isnan(spearmanr(p,yt[testcv])[0]):
               sp+=spearmanr(p,yt[testcv])[0]
               lintr+=1
        #print s1_prediction
        if lintr>0:
           if not math.isnan(sp/lintr):
              tot_sp=float(tot_sp)+float(sp/lintr)
              intr+=1
        if intr>0:
           print tot_sp/intr
    print tot_sp/intr
    '''

    tot_sp=0
    intr=0
    avg_c={}
    s2_prediction=s1_prediction
    for cc in compl2:
        for c in s1_prediction[priority[0]]:
            avg_c[cc]=0
            prot=0
            for p in compl2[cc]:
                if p in s1_prediction:
                   avg_c[cc]+=s1_prediction[p][c]
                   prot+=1
            if prot > 0:
               avg_c[cc]=avg_c[cc]/prot
            for p in compl2[cc]:
                if p in s2_prediction:
                   s2_prediction[p][c]=avg_c[cc]

    for g in s2_prediction:
        row=[]
        yt=np.array(essent2[g])
        for c in s2_prediction[g]:
            row.append(s2_prediction[g][c])
        p1=np.array(row)
        if not (len(yt)==len(p1)):
           print g
           continue
        sp=spearmanr(p1,yt)[0]
        tot_sp=float(tot_sp)+float(sp)
        intr+=1
    print "stage3 avg sp:",
    print tot_sp/intr

    #test compl distr
    nline=0
    essent={}
    min_es=100
    max_es=-100
    headers3=[]
    whole_avg=0
    val=[]
    for line in open(fname2):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers3.append(h)
               essent[h]={}
        else:
             x=0
             row3=[ccel[1]]
             for data in ccel:
                 if x>=2 and ccel[1] in all_info:
                    essent[headers[x]][ccel[1]]=float(data)
                    val.append(float(data))
                    whole_avg+=float(data)
                    if float(data)<min_es:
                       min_es=float(data)
                    if float(data)>max_es:
                       max_es=float(data)
                 x+=1
        nline+=1
        

    print whole_avg/(nline*(len(headers3)-2))
    nline=0
    expression={}
    min_es=100
    max_es=-100
    headers3=[]
    whole_avg=0
    val_expr=[]
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers3.append(h)
               expression[h]={}
        else:
             x=0
             row3=[ccel[1]]
             for data in ccel:
                 if x>=2 and ccel[1] in all_info:
                    expression[headers[x]][ccel[1]]=float(data)
                    val_expr.append(float(data))
                 x+=1
        nline+=1

    nline=0
    copynumber={}
    min_es=100
    max_es=-100
    headers3=[]
    whole_avg=0
    val_cnv=[]
    for line in open(fname_cnv):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers3.append(h)
               copynumber[h]={}
        else:
             x=0
             row3=[ccel[1]]
             for data in ccel:
                 if x>=2 and ccel[1] in all_info:
                    copynumber[headers[x]][ccel[1]]=float(data)
                    val_cnv.append(float(data))
                 x+=1
        nline+=1

    sum=0
    sum2=0
    sum3=0
    tot=0
    print min_es
    print max_es
    low_s=[]
    l_sd={}
    pout={}
    pin={}
    avg_ar=[]
    std_ar=[]
    
    #check ppi centrality vs essentiality

    #check ggi centrality vs essentiality
    
    #check essentiality vs cancer essentiality
    
    #check mutations vs essentiality
    


    for c in compl2:
        l_sd[c]=[]
    for i in headers3:
        sum=0
        sum2=0
        sum3=0
        tot=0
        pout[i]={}
        pin[i]={}
        avg_ar=[]
        std_ar=[]
        ravg_ar=[]
        rstd_ar=[]
        avg_ar_expr=[]
        std_ar_expr=[]
        ravg_ar_expr=[]
        rstd_ar_expr=[]
        avg_ar_cnv=[]
        std_ar_cnv=[]
        ravg_ar_cnv=[]
        rstd_ar_cnv=[]
        print ""
        print ""
        for c in compl2:
            avg=0
            std=0
            avg2=0
            std2=0
            avg_expr=0
            std_expr=0
            avg_cnv=0
            std_cnv=0
            less=0
            r_l=[]
            r_le=[]
            r_lc=[]
            #print str(c),
            for p in compl2[c]:
                if p in essent[i]:
                  avg+=essent[i][p]
                  #print "\t"+str(essent[i][p]),
                  #avg_exp+=float(expr[str(i)+str(p)])
                  r=random.uniform(min_es, max_es)
                  avg2+=r
                  r_l.append(r)
                if p in expression[i]:
                  avg_expr+=expression[i][p]
                  r=random.uniform(min_es, max_es)
                  avg2+=r
                  r_le.append(r)
                if p in copynumber[i]:
                  avg_cnv+=copynumber[i][p]
                  r=random.uniform(min_es, max_es)
                  avg2+=r
                  r_lc.append(r)

            if len(r_l)<=1:
               continue
            pin[i][c]=[]
            pout[i][c]=[]
            #print str(c)+"\t"+str(i)
            avg=avg/len(r_l)
            for p in compl2[c]:
                if p in essent[i]:
                   std+=(essent[i][p]-avg)*(essent[i][p]-avg)
                if p in expression[i]:
                   std_expr+=(expression[i][p]-avg_expr)*(expression[i][p]-avg_expr)
                if p in copynumber[i]:
                   std_cnv+=(copynumber[i][p]-avg_cnv)*(copynumber[i][p]-avg_cnv)
                   #std_exp+=float(expr[str(i)+str(p)])
            for p in compl2[c]:
                if p in essent[i]:
                   if (essent[i][p]-avg)*(essent[i][p]-avg)>(std/len(r_l)):
                      pout[i][c].append(p)
                      #print str(p)
                   else:
                        pin[i][c].append(p)
            for r in r_l:
                std2+=(r-avg2)*(r-avg2)

            #r_l2=np.random.normal(0.0012, 0.4883, len(r_l)) #these are the parameters for essentiality
            r_l2=random.sample(val, len(r_l)) #these are the parameters for expression
            r_l2_expr=random.sample(val_expr, len(r_le)) #these are the parameters for expression
            r_l2_cnv=random.sample(val_cnv, len(r_lc)) #these are the parameters for expression
            if len(r_l)>1:
              std=math.sqrt(std/len(r_l))
              l_sd[c].append(avg)
              std2=math.sqrt(std2/len(r_l))
              #sum+=std-std2
              avg_ar.append(avg)
              std_ar.append(std)
              ravg_ar.append(np.mean(r_l2))
              rstd_ar.append(np.std(r_l2))
              avg_ar_expr.append(avg_expr)
              std_ar_expr.append(std_expr)
              ravg_ar_expr.append(np.mean(r_l2_expr))
              rstd_ar_expr.append(np.std(r_l2_expr))
              avg_ar_cnv.append(avg_cnv)
              std_ar_cnv.append(std_cnv)
              ravg_ar_cnv.append(np.mean(r_l2_cnv))
              rstd_ar_cnv.append(np.std(r_l2_cnv))
              if std<np.std(r_l2):
                 #print str(c)+"\t"+str(std)+"\t"+str(avg)
                 less+=1
              sum+=std-np.std(r_l2)
              #if std-np.std(r_l2)<0 and std<0.01:
                 #print str(c)+"\t"+str(len(compl2[c]))+"\t"+str(std)+"\t"+str(avg)

                 #low_s[c]+=1
              sum2+=avg
              #sum3+=avg2
              sum3+=np.mean(r_l2)
              tot+=1
        if tot>0:
           #a=np.array(avg_ar)
           #b=np.array(std_ar)
           #print pearsonr(a,b)
           print sum/(tot)
           print sum2/(tot)
           print sum3/(tot)
           print tot

    a=np.array(avg_ar)
    b=np.array(std_ar)
    c=np.array(ravg_ar)
    d=np.array(rstd_ar)
    print pearsonr(a,b)
    print pearsonr(c,d)
    print pearsonr(a,c)
    print pearsonr(b,d)
    ax=np.array(avg_ar_cnv)
    bx=np.array(std_ar_cnv)
    cx=np.array(ravg_ar_cnv)
    dx=np.array(rstd_ar_cnv)
    

    print pearsonr(ax,a)
    print pearsonr(bx,a)
    print pearsonr(cx,a)
    print pearsonr(dx,a)
    print pearsonr(ax,c)
    print pearsonr(bx,c)
    print pearsonr(cx,c)
    print pearsonr(dx,c)


    val_arr=np.array(val)
    val_expr_arr=np.array(val_expr)
    val_cnv_arr=np.array(val_cnv)
    print pearsonr(val_arr,val_expr_arr)
    print pearsonr(val_arr,val_cnv_arr)
    print pearsonr(val_cnv_arr,val_expr_arr)
    expr_d=[]
    essent_d=[]
    for c in headers:
        for g in geneInEssent:
            if str(c)+str(g) in expr:
               expr_d.append(expr[str(c)+str(g)])
               essent_d.append(essent[c][g])
    a=np.array(expr_d)
    b=np.array(essent_d)
    print pearsonr(a,b)
    

    print sum/(tot)
    print sum2/(tot)
    print sum3/(tot)
    print tot
    #for i in pout:
    #    for c in pout[2]:

    #for c in compl2:
    #    print l_sd[c]


    print "parsing done"
    return tmap


def read_matrix2(fname, fname2, fname3, fname_cnv, ge,es,ge_t, n_gene, n_ccel, ccel_f, t_exp, compl, ccel_ann, mutations, smutations, cancer_genes,ge2, headers2, trainingMapping):

    nline=0
    id_type = []
    id_site = []
    id_site2 = []
    id_hist = []
    id_hists = []

    ccel_type = {}
    ccel_site = {}
    ccel_site2 = {}
    ccel_gender = {}
    ccel_hist = {}
    ccel_hists = {}
    for line in open(ccel_f):
        line=line.replace("\n", "")
        line=line.replace("\r", "")

        ccel=line.split('\t')
        if not nline==0:
           ccel_type[ccel[0]]=ccel[2]
           ccel_site[ccel[0]]=ccel[3]
           found=False
           if ccel[0] in ccel_ann:
              found=True
              ccel_gender[ccel[0]]=ccel_ann[ccel[0]][0]
              ccel_site2[ccel[0]]=ccel_ann[ccel[0]][1]
              ccel_hist[ccel[0]]=ccel_ann[ccel[0]][2]
              ccel_hists[ccel[0]]=ccel_ann[ccel[0]][3]
           if ccel[1] in ccel_ann and not found:
              found=True
              ccel_gender[ccel[0]]=ccel_ann[ccel[1]][0]
              ccel_site2[ccel[0]]=ccel_ann[ccel[1]][1]
              ccel_hist[ccel[0]]=ccel_ann[ccel[1]][2]
              ccel_hists[ccel[0]]=ccel_ann[ccel[1]][3]
           if not found:
              print "MISSING!"+ccel[0]
              ccel_gender[ccel[0]]=''
              ccel_site2[ccel[0]]=''
              ccel_hist[ccel[0]]=''
              ccel_hists[ccel[0]]=''

        nline+=1
    for s in ccel_site:
        if not (ccel_site[s] in id_site):
           id_site.append(ccel_site[s])

    for s in ccel_type:
        if not (ccel_type[s] in id_type):
           id_type.append(ccel_type[s])

    headers = []
    nline=0
    expr = {}
    out_file = open("test_x.txt","w")
    out_file2 = open("test_y.txt","w")
    exp_avg = 0
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data
                    expr[str(headers[x])+str(ccel[1])]=data
                    exp_avg+=float(data)
                 x+=1
        nline+=1
    exp_avg=exp_avg/len(expr)
    print exp_avg
    exp_std=0
    for e in expr:
        exp_std+=(float(expr[e])-exp_avg)*(float(expr[e])-exp_avg)
    exp_std=math.sqrt(exp_std/len(expr))
    print exp_std
    headers = []
    nline=0
    cnv = {}
    for line in open(fname_cnv):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers2.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data
                    cnv[str(headers2[x])+str(ccel[1])]=data
                 x+=1
        nline+=1

    headers = []
    nline=0
    tmap={}
    g_counter=0
    out_file.write("Gene\tCCEL\tGene Expression\tCNV\tType1\tSite1\tGender\tSite2\tHistology\tHistology subtype\t# SNPs\t# structural mutations\tExpressed in site\tKnown cancer gene\tEssentiality\n")
    for line in open(fname):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           for h in ccel:
               headers.append(h)
        else:
             x=0
             for data in ccel:
                 #if x>=2 and str(headers[x])+str(ccel[1]) in expr and  str(headers[x])+str(ccel[1]) in cnv and ccel[1] in trainingMapping:
                 if x>=2 and str(headers[x])+str(ccel[1]) in expr and  str(headers[x])+str(ccel[1]) in cnv:
                    expr_in_site=0
                    #print ccel[1]
                    tmap[g_counter]=ccel[1]
                    g_counter+=1
                    if ccel[1] in t_exp:
                      if ccel_site[str(headers[x])] in t_exp[ccel[1]]:
                         expr_in_site=1
                      #print ccel[1]+" ",
                      #print ccel_site[str(headers[x])]+" ",
                      #for t in t_exp[ccel[1]]:
                          #print t+" ",
                      #print ""
                    isCancerGene=0
                    if ccel[1] in cancer_genes:
                       isCancerGene=1
                    #c=0
                    #if ccel[1] in compl:
                    #   c=compl[ccel[1]]
                    n_mutations=0
                    if (str(headers[x]) in mutations):
                       if ccel[1] in mutations[str(headers[x])]:
                          n_mutations=mutations[str(headers[x])][ccel[1]]

                    if not (str(headers[x]) in smutations):
                       smutations[str(headers[x])]=0
                    out_file.write(ccel[1]+"\t"+headers[x]+"\t"+expr[str(headers[x])+str(ccel[1])]+"\t"+str(cnv[str(headers[x])+str(ccel[1])])+"\t"+str(ccel_type[str(headers[x])])+"\t"+str(ccel_site[str(headers[x])])+"\t"+str(ccel_gender[str(headers[x])])+"\t"+str(ccel_site2[str(headers[x])])+"\t"+str(ccel_hist[str(headers[x])])+"\t"+str(ccel_hists[str(headers[x])])+"\t"+str(n_mutations)+"\t"+str(smutations[str(headers[x])])+"\t"+str(expr_in_site)+"\t"+str(isCancerGene)+"\t"+data+"\n")
                    out_file2.write(data+"\t")
                    full=[nline,x]
                    #for ex in expr:
                        #full.append(expr[ex])
                    #ge2.append([nline,x,float(expr[str(headers[x])+str(ccel[1])])])
                    ge2.append(full)
                    #ge2.append([float(expr[str(headers[x])+str(ccel[1])])])
                    row=[float(expr[str(headers[x])+str(ccel[1])]),float(cnv[str(headers[x])+str(ccel[1])])]
                    tissID=0
                    for id1,t in enumerate(id_type):
                        if t==str(ccel_type[str(headers[x])]):
                           #row.append(1)
                           tissID=id1
                        #else:
                             #row.append(0)
                    row.append(tissID)
                    siteID=0
                    for id1,s in enumerate(id_site):
                        if s==str(ccel_site[str(headers[x])]):
                            siteID=id1
                    #row.append(siteID)
                    gender=0
                    if ccel_gender[str(headers[x])]=='M':
                       gender=1
                    if ccel_gender[str(headers[x])]=='F':
                       gender=2
                    #row.append(gender)
                    siteID=0
                    for id1,s in enumerate(id_site2):
                        if s==str(ccel_site2[str(headers[x])]):
                            siteID=id1
                    row.append(siteID)


                    siteID=0
                    for id1,s in enumerate(id_hist):
                        if s==str(ccel_hist[str(headers[x])]):
                            siteID=id1
                    row.append(siteID)

                    siteID=0
                    for id1,s in enumerate(id_hists):
                        if s==str(ccel_hists[str(headers[x])]):
                            siteID=id1
                    row.append(siteID)

                    row.append(n_mutations)
                    row.append(smutations[str(headers[x])])
                    #row.append(expr_in_site)
                    row.append(isCancerGene)
                    ge.append(row)
                    #ge.append([float(expr[str(headers[x])+str(ccel[1])]),float(cnv[str(headers[x])+str(ccel[1])]),str(ccel_type[str(headers[x])]),str(ccel_site[str(headers[x])])])
                    es.append(float(data))
                    #print ccel[1]+"\t"+headers[x]+"\t"+expr[str(headers[x])+str(ccel[1])]+"\t"+data
                 x+=1
             out_file2.write("\n")

        nline+=1

    nline=0
    for line in open(fname3):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        ccel=line.split('\t')
        if nline==0:
           a=0
           #for h in ccel:
               #headers.append(h)
        else:
             x=0
             for data in ccel:
                 if x>=2:
                    #print ccel[1]+"\t"+headers[x]+"\t"+data

                    ge_t.append([float(data)])
                 x+=1
             ccel=x
        nline+=1
    n_gene=nline
        
    print "parsing done"
    return tmap

def read_fun(fname):
    fun = {}
    for line in open(fname):
        words = line.split(' ',1)
        fun[words[0]] = words[1]
    return fun
    
def scf (expected,prediction):
    print len(prediction)
    print len(expected)
    par=0
    par2=0
    p_m=prediction.mean()
    e_m=expected.mean()
    print p_m
    print e_m
    p=0
    p2=0
    p3=0
    avg_error=0
    for i in range(0,len(prediction)-1):
        #print str(prediction[i])+"-"+str(expected[i])
        avg_error+=abs(prediction[i]-expected[i])
        p+=(float(prediction[i])-p_m)*(float(expected[i])-e_m)
        p2+=(float(prediction[i])-p_m)*(float(prediction[i])-p_m)
        p3+=(float(expected[i])-e_m)*(float(expected[i])-e_m)
        par+=(float(prediction[i])-float(expected[i]))*(float(prediction[i])-float(expected[i]))
        par2+=abs(float(prediction[i])-float(expected[i]))
    #print par
    r=p/(math.sqrt(p2)*math.sqrt(p3))
    print "avg error:"+str(avg_error/len(prediction))
    #print par2
    #sp=1-((6*par)/(len(prediction)*(len(prediction)*len(prediction)-1)))
    return r



def abd(argv):

    tissue_exp = read_texp(sys.argv[6],sys.argv[7])
    unip2gene = read_unip(sys.argv[8])
    unip2gene_int = read_unip(sys.argv[18])
    compl2 = {}
    whole_ccel_ann = read_ccel(sys.argv[10])
    mutations = count_mutations(sys.argv[11])
    smutations = count_smutations(sys.argv[12])
    cancer_genes = cancerGenes(sys.argv[13])
    compl = read_compl(sys.argv[9],unip2gene,compl2)
    compl2_c1={}
    compl_c1 = read_compl_c1(sys.argv[19],unip2gene,compl2_c1)
    interactions = read_int(sys.argv[17],unip2gene_int)
    #interactions={}
    ppi=read_int(sys.argv[18],unip2gene_int)
    priority = []
    ge=[]
    es=[]
    ge2=[]
    ge_t=[]
    n_gene=0
    n_ccel=0
    ge3=[]
    essent={}
    geneInEssent=[]
    priority=[]
    print sys.argv[16]
    for line in open(sys.argv[16]):
        line=line.replace("\n", "")
        line=line.replace("\r", "")
        priority.append(line)

    trainingMapping = read_matrix(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],ge,es,ge_t, n_gene, n_ccel,sys.argv[5], tissue_exp, compl, whole_ccel_ann,mutations,smutations, cancer_genes,ge2,ge3,essent,geneInEssent,sys.argv[14], priority, compl2, interactions, compl_c1, ppi)
    #most={}
    #for g in priority:
    #    most[g]={}
    #    for g2 in essent:
    #        most[g][g2]=spearmanr(essent[g],essent[g2])

    #print ge2
    X = np.array(ge)
    X2 = np.array(ge2)
    X3 = np.array(ge3)
    #print X
    y = np.array(es)
    headers=[]
    ge_t=[]
    testMapping = read_matrix2(sys.argv[3],sys.argv[2],sys.argv[1],sys.argv[14],ge_t,es,ge, n_gene, n_ccel,sys.argv[15], tissue_exp, compl2, whole_ccel_ann,mutations,smutations, cancer_genes,ge2,headers, geneInEssent)
    test = np.array(ge_t)
    print headers

    print len(headers)
    #print "ge:"+str(len(ge))+"y:"+str(len(es))+"test:"+str(len(ge_t))

    #print X
    #kf = KFold(len(y), n_folds=2)

    #X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.4, random_state=0)

    knn = neighbors.KNeighborsRegressor(n_neighbors=11)
    knn.fit(X,y)
    res=knn.predict(test)
    for c in headers:
        print "\t"+str(c),
    r_c=0
    for p in res:
        if r_c%(len(headers)-2)==0:
           print ""
           print testMapping[r_c]+"\t"+testMapping[r_c],
        print "\t"+str(p),
        r_c+=1



    #nline=0
    #for r in res:
        #print str(r)+"\t",
        #nline+=1
        #if nline%22==0:
           #print ""
    #print res
    #for train, test in kf:
        #y_=knn.fit(train,test)
        #results.append( myEvaluationFunc(es[test], [x[1] for x in mdl]) )
    #knn.fit(X,y)
    #print mdl.score(X,y)
    #res=knn.predict(test)
    #for p in res:
    #    print p

    kf = KFold(len(y), n_folds=10)
    knn2 = neighbors.KNeighborsRegressor(n_neighbors=11)
    knn3 = neighbors.KNeighborsRegressor(n_neighbors=11)
    knn4 = neighbors.KNeighborsRegressor(n_neighbors=11)

    #gnb = GaussianNB()
    results=[]
    #scores = cross_validation.cross_val_score(knn2, X, y, cv=5)
    #print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

    for traincv, testcv in kf:
        #probas=gnb.fit(X[traincv],y[traincv]).predict(X[testcv])
        #print "fold results: ",
        #s=scf(y[testcv], probas)
        #print s

        probas=knn2.fit(X[traincv],y[traincv]).predict(X[testcv])

        #for i in testcv:
        #    res_tmp[X[0][i]].append(y[i])
        #    res_tmp2[X[0][i]].append(y[i])
        #for t in X:
        #    if not (t in X[traincv]):
        #       if t[0] in res_tmp:
        #          res_tmp[t[0]].append(t[9])
        #       else:
        #            res_tmp[t[0]]=[]




        #probas3=knn4.fit(X3[traincv],y[traincv]).predict(X3[testcv])
        print str(len(y[testcv]))+"fold results: ",
        print spearmanr(y[testcv], probas)
        print pearsonr(y[testcv], probas)
        s=scf(y[testcv], probas)
        print s
        results.append( s )



        res_tmp={}
        res_tmp2={}
        avg_error=0
        for i,p in enumerate(probas):
            ind=testcv[i]
            if not (trainingMapping[ind] in res_tmp):
               res_tmp[trainingMapping[ind]]=[]
               res_tmp2[trainingMapping[ind]]=[]

            res_tmp[trainingMapping[ind]].append(p)
            res_tmp2[trainingMapping[ind]].append(y[ind])
            avg_error+=abs(p-y[ind])

        print avg_error/len(testcv)
        avg_s=0
        for g in res_tmp:
            avg_s+=spearmanr(res_tmp[g],res_tmp2[g])[0]
            #print spearmanr(res_tmp[g],res_tmp2[g])[0]
        print "avg spearman's corr"+str(avg_s/len(res_tmp))

        probas2=knn3.fit(X2[traincv],y[traincv]).predict(X2[testcv])
        print "fold results2: ",
        print spearmanr(y[testcv], probas2)
        print pearsonr(y[testcv], probas2)
        print ""



        #for idc,c in enumerate(compl):
        #    knn3 = neighbors.KNeighborsRegressor()
        #    new_x = []
        #    #get prediction within compl
        #    ccel_c={}
        #    for id1,features in enumerate(X):
        #        if not (features[1] in ccel_c):
        #           ccel_c[features[1]]=[features[1]]
        #        if features[0] in c:
        #           ccel_c[features[1]].append(probas[id1])
        #           #new_x.append([probas[id1]])
        #    for c in ccel_c:
        #        new_x.append(c)
        #
        #    compl_training=



    print np.array(results).mean()
    

    res=knn.fit(X,y).predict(test)



    #res=knn2.predict(test)
    #for p in res:
    #    print p

    #print mdl

    
    results=[]
    #regr = LinearRegression()
    #for traincv, testcv in kf:
        #probas=regr.fit(X[traincv],y[traincv]).predict(X[testcv])
        #s=scf(y[testcv], probas)
        #print s
        #results.append( s )
    #print np.array(results).mean()

    #prova modifica
    results=[]
    nn = BernoulliRBM()
    for traincv, testcv in kf:
        probas=nn.fit(X[traincv],y[traincv]).predict(X[testcv])
        s=scf(y[testcv], probas)
        print s
        results.append( s )

    print np.array(results).mean()

    return 0


def main(argv):
    return abd(argv)


if __name__ == "__main__":
   main(sys.argv[1:])
